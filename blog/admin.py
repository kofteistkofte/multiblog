from django import forms
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from blog.models import BlogPost


class PostForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorWidget)

    class Meta:
        model = BlogPost
        fields = "__all__"


@admin.register(BlogPost)
class PostAdmin(admin.ModelAdmin):
    form = PostForm
