# Generated by Django 2.2.1 on 2019-05-18 13:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('sites', '0002_alter_domain_unique'),
    ]

    operations = [
        migrations.CreateModel(
            name='BlogPost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='Title')),
                ('url', models.SlugField(blank=True, max_length=220, null=True, verbose_name='URL')),
                ('thumbnail', models.ImageField(blank=True, null=True, upload_to='thumbnails/%Y/%m', verbose_name='Thumbnail')),
                ('keywords', models.CharField(blank=True, max_length=1000, null=True, verbose_name='Keywords')),
                ('body', models.TextField(verbose_name='Content')),
                ('pub_time', models.DateTimeField(blank=True, null=True, verbose_name='Publish Time')),
                ('site', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sites.Site', verbose_name='Site')),
            ],
            options={
                'verbose_name': 'Blog Post',
                'verbose_name_plural': 'Blog Posts',
                'ordering': ['pub_time'],
            },
        ),
    ]
