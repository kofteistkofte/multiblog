from django.views import generic

from blog.models import BlogPost


class PostDetailView(generic.DetailView):
    template_name = "post.html"
    model = BlogPost
    slug_field = "url"
    context_object_name = "post"
