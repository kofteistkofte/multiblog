import re
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.contrib.sites.models import Site
from django.utils.translation import gettext as _

from coresite.models import ContentBase, PromotedDomain


class BlogPost(ContentBase):
    site = models.ForeignKey(Site, verbose_name=_("Site"), on_delete=models.CASCADE)
    pub_time = models.DateTimeField(_("Publish Time"), null=True, blank=True)
    featured = models.BooleanField(_("Featured"), default=False)

    class Meta:
        verbose_name = _("Blog Post")
        verbose_name_plural = _("Blog Posts")
        ordering = ['featured', '-pub_time']

    def __str__(self):
        return f"{self.title} - {self.site}"

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if not self.pub_time:
            self.pub_time = timezone.now()

    def get_absolute_url(self):
        return reverse('blog:detail', args=[str(self.url)])

    def get_body(self):
        raw = self.body
        domain = PromotedDomain.objects.filter(active=True).first()
        with_domain = re.sub('\{\{ (domain) \}\}',
                             r'<a href="' + domain.domain + '">' + domain.name + '</a>',
                             raw)
        return with_domain
