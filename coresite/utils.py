from django.utils.text import slugify


def slug_generator(*args):
    """
    A simple function to generate slugs.

    To use it call it as 'slug_generator(self)'. Argument 'self' is usual argument for classes.

    It takes the output of __str__ function of current class and uses django slugify function to
    turn it into a slug.
    Before giving an output, function tries to make sure if there is a object with same slug.
    If there is it recreates the slug with a number added at the end till makes sure that slug is unique.
    """
    self = args[0]
    temp_slug = slugify(self.title)
    in_slug = temp_slug
    add_num = None
    while True:
        if add_num:
            in_slug = temp_slug + str(add_num)
        try:
            is_slug = self.__class__.objects.get(url=in_slug)
        except Exception:
            is_slug = None
        if not is_slug:
            return in_slug
            break
        elif is_slug and add_num:
            add_num += 1
        else:
            add_num = 1
        continue
