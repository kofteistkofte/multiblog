from re import split
from django.db import models
from django.contrib.sites.models import Site
from django.utils.translation import gettext as _

from coresite.utils import slug_generator


class ContentBase(models.Model):
    """ An abstract model for content models like pages, blog posts etc.. """
    title = models.CharField(_("Title"), max_length=200)
    url = models.SlugField(_("URL"), max_length=220, null=True, blank=True)
    thumbnail = models.ImageField(_("Thumbnail"), upload_to="thumbnails/%Y/%m", null=True, blank=True)
    keywords = models.CharField(_("Keywords"), max_length=1000, null=True, blank=True)
    body = models.TextField(_("Content"))

    class Meta:
        abstract = True

    def __str__(self):
        return self.title

    def get_tag_list(self):
        """
        A basic regular expression for converting keywords field to a list.
        """
        if self.keywords:
            return split(', ?', self.keywords)

    def save(self, *args, **kwargs):
        # Checks if there is a slug, if not calls slug_generator.
        if not self.url:
            self.url = slug_generator(self)
        super().save(*args, **kwargs)


class SiteSettings(models.Model):
    site = models.OneToOneField(Site, verbose_name=_("Site"),
                                related_name="settings",
                                on_delete=models.CASCADE)
    name = models.CharField(_("Site Name"), max_length=100)
    desc = models.CharField(_("Site Description"), max_length=400, null=True, blank=True)

    class Meta:
        verbose_name = _("Site Configs")
        verbose_name_plural = _("Site Configs")

    def __str__(self):
        return "Site Ayarları"


class PromoteLinks(models.Model):
    name = models.CharField(_("Name"), max_length=128)
    url = models.URLField(_("URL"))

    class Meta:
        verbose_name = _("Promotion Link")
        verbose_name_plural = _("Promotion Links")

    def __str__(self):
        return self.name


class PromotedDomain(models.Model):
    name = models.CharField(_("Name"), max_length=128)
    domain = models.URLField(_("Domain"))
    active = models.BooleanField(_("Active"), default=True)

    class Meta:
        verbose_name = _("Promoted Domain")
        verbose_name_plural = _("Promoted Domains")

    def __str__(self):
        return self.domain
