from django.contrib import admin
from django.contrib.sites.models import Site
from django.contrib.sites.admin import SiteAdmin as BaseSiteAdmin

from coresite.models import SiteSettings, PromoteLinks, PromotedDomain

admin.site.unregister(Site)


class SiteSettingsInline(admin.TabularInline):
    model = SiteSettings
    extra = 1
    max_num = 1


@admin.register(Site)
class SiteAdmin(BaseSiteAdmin):
    inlines = [SiteSettingsInline]


@admin.register(PromotedDomain)
class PromotedDomainAdmin(admin.ModelAdmin):
    pass


@admin.register(PromoteLinks)
class PromoteLinksAdmin(admin.ModelAdmin):
    pass
