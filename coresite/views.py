from django.views import generic
from django.contrib.sites.shortcuts import get_current_site

from blog.models import BlogPost


class IndexView(generic.TemplateView):
    """ Content used by both IndexTemplateView and IndexListView """
    template_name = "index.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        current_site = get_current_site(self.request)
        context['posts'] = BlogPost.objects.filter(site=current_site)
        return context
