from django.urls import path

from coresite import views

app_name = "coresite"


urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
]
