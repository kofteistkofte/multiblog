from django import template
from coresite.models import SiteSettings

register = template.Library()


def sitesettings():
    try:
        return SiteSettings.objects.first()
    except Exception:
        pass


@register.simple_tag
def get_name():
    try:
        if sitesettings().name:
            return sitesettings().name
        else:
            return "Deneme Site"
    except Exception:
        return "Deneme Site"


@register.simple_tag
def get_desc():
    try:
        if sitesettings().desc:
            return sitesettings().desc
        else:
            return "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
    except Exception:
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
