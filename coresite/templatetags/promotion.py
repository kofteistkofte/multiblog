from django import template
from coresite.models import PromoteLinks

register = template.Library()


@register.simple_tag
def get_links():
    return PromoteLinks.objects.all()
